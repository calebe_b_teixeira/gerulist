# Gerulist

Olá pessoal da GERU aqui está a minha solução para o "challenge" de vocês.

Gostaria de implementar mais detalhes, porém o tempo não me permitiu. (Estou em um momento de transição, por isso não pude dar tanto tempo quanto quis a esse job);

Estou utilizando um "database fake". A ideia éra utilizar o Firebase, o que também não consegui tempo.

Espero que gostem! :)

**Aqui vai a lista do que foi implementado:**

*Tecnologias utilizadas*
- Vue.js
- Nuxt
- Bootstrap
- SCSS
- Javascript
- Lodash

*Funcionalidade da Lista*
- Lista padrão (Inbox)
- Criar nova lista
- Deletar lista
- Adicionar tarefa à lista
- Ver/Esconder tarefas finalizadas

*Filtros de tarefas*
- Starred
- Today
- Week
- Month

*Funcionalidade da Tarefa*
- Criar
- Editar
- Deletar
- Marcar como tarefa completa
- Marcar como importante (aparece na lista 'starred')
- Definir data de término
- Definir dia para lembrar da tarefa

*Funcionalidade da Subtarefa*
- Criar
- ~~Editar~~
- ~~Deletar~~

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
