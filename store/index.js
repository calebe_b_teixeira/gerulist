import Vuex from 'vuex';
import state from '@/store/state.js';
import getters from '@/store/getters.js';
import actions from '@/store/actions.js';

const createStore = () => {
    return new Vuex.Store({
      state: state,
      getters: getters,
      actions: actions
    })
}

export default createStore;