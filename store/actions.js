export default {
  addSubtask(vuexContext, data) {
    const task = vuexContext.getters.task(data.taskId);
    task.subtasks.push(data.subtask);
  },
  addTask(vuexContext, task) {
    const list = vuexContext.getters.list(task.listId);
    list.tasks.push(task);
  },
  deleteTask(vuexContext, task) {
    const list = vuexContext.getters.list(task.listId);
    list.tasks.splice(task.id, 1);
  },
  toggleSubtaskStatus(vuexContext, subtask) {
    subtask.completed = !subtask.completed;
  },
  toggleTaskStatus(vuexContext, task) {
    task.completed = !task.completed;
    task.completionDate = task.completed ? new Date().getTime() : null;
  },
  toggleTaskStar(vuexContext, task) {
    task.starred = !task.starred;
  },
  addList(vuexContext, list) {
    const lists = vuexContext.getters.lists;
    const newList = {
      "id": list.id,
      "title": list.title,
      "inbox": false,
      "tasks": []
    };
    lists.push(newList);
  },
  deleteList(vuexContext, listId) {
    const lists = vuexContext.getters.lists;

    const index = _.findIndex(lists, l => {
      return l.id === listId;
    });

    lists.splice(index, 1);
  }
}