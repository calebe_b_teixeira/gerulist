import _ from 'lodash';
import moment from 'moment';

export default {
  randomId () {
    return () => {
      let length = 10;
      let id = '';
      let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      for (let i = 0; i < length; i++) {
        id += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      return id;
    }
  },
  lists (state) {
    return state.lists
  },
  listsWithoutInbox (state) {
    return _.filter(state.lists, o => !o.inbox)
  },
  list (state, getters) {
    return (listId) => {
      return _.find(getters.lists, { id: listId });
    };
  },
  inboxList (state) {
    return _.find(state.lists, { inbox: true });
  },
  allTasks (state, getters) {
    const lists = getters.lists;
    const allTasks = [];
    _.each(lists, list => allTasks.push(...list.tasks));
    return allTasks
  },
  task (state, getters) {
    return (taskId) => {
      return _.find(getters.allTasks, { id: taskId });
    }
  },
  taskIndex (state, getters) {
    return (data) => {
      const list = getters.list(data.listId);
      return _.findIndex(list.tasks, { id: data.taskId });
    }
  },
  starredList (state, getters) {
    const starredTasks = _.filter(getters.allTasks, { starred: true });
    return { "tasks": starredTasks }
  },
  todayList (state, getters) {
    const todayTasks = [];
    _.each(getters.allTasks, (task) => {
      let date = task.dueDate;
      if(date) {
        let diff = Math.round(moment().diff( date, 'day', true));
        if(_.inRange(diff, 0, Number.MAX_SAFE_INTEGER)) {
          // Number.MAX_SAFE_INTEGER to include all late task
          // console.log(date.toString() + ' -> ' + diff);
          todayTasks.push(task);
        }
      }
    });
    return { "tasks": todayTasks }
  },
  weekList (state, getters) {
    const weekTasks = [];
    _.each(getters.allTasks, (task) => {
      let date = task.dueDate;
      if(date) {
        let diff = Math.round(moment().diff( date, 'day', true));
        if(_.inRange(diff, -6, Number.MAX_SAFE_INTEGER)) {
          // Number.MAX_SAFE_INTEGER to include all late task
          // console.log(date.toString() + ' -> ' + diff);
          weekTasks.push(task);
        }
      }
    });
    return { "tasks": weekTasks }
  },
  monthList (state, getters) {
    const monthTasks = [];
    _.each(getters.allTasks, (task) => {
      let date = task.dueDate;
      if(date) {
        let diff = Math.round(moment().diff( date, 'day', true));
        if(_.inRange(diff, -31, Number.MAX_SAFE_INTEGER)) {
          // Number.MAX_SAFE_INTEGER to include all late task
          // console.log(date.toString() + ' -> ' + diff);
          monthTasks.push(task);
        }
      }
    });
    return { "tasks": monthTasks }
  },
  getFriendlyDate(state, getters) {
    return (date) => {
      return moment(date).format('ddd, MMMM DD');
    }
  },
  getComputerDate(state, getters) {
    return (date) => {
      let t = moment().format('HH:MM:SS');
      let d = moment(date).format('YYYY-MM-DD');
      return `${d} ${t}`;
    }
  },
  isDateInPast(state, getters) {
    return (date) => {
      let diff = moment().diff(date, 'day');
      return diff > 0;
    }
  },
};