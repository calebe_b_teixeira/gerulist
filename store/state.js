export default {
  lists: [
    {
      "id": "inbox",
      "title": "Inbox",
      "inbox": true,
      "tasks": []
    },
    {
      "id": "cCbGroWCzi",
      "title": "New Hotsite",
      "tasks": [
        {
          "id": "CV4HUSHpm8",        
          "listId": "cCbGroWCzi",
          "title": "Update Wordpress",
          "creationDate": "Mon Oct 01 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "completionDate": "Wed Oct 10 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "dueDate": null,
          "remindDate": null,
          "description": null,
          "completed": true,
          "starred": true,
          "subtasks": []
        },
        {
          "id": "OuS1nlq4lr",
          "listId": "cCbGroWCzi",
          "title": "Fix removed emails bug",
          "creationDate": "Thu Oct 04 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "completionDate": null,
          "dueDate": null,
          "remindDate": null,
          "description": null,
          "completed": false,
          "starred": false,
          "subtasks": []
        },
        {
          "id": "PeRnIR8YAf",
          "listId": "cCbGroWCzi",
          "title": "Check website´s SEO on Scraming Frog",
          "creationDate": "Mon Oct 08 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "completionDate": "Wed Oct 10 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "dueDate": null,
          "remindDate": null,
          "description": null,
          "completed": true,
          "starred": false,
          "subtasks": []
        },
        {
          "id": "J77Kjez8y8",
          "listId": "cCbGroWCzi",
          "title": "Change encode from javascript file",
          "creationDate": "Sat Oct 13 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "completionDate": "Sun Oct 14 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "dueDate": null,
          "remindDate": null,
          "description": null,
          "completed": true,
          "starred": true,
          "subtasks": []
        },
        {
          "id": "Xx1LRYqhkx",
          "listId": "cCbGroWCzi",
          "title": "Change images from blog",
          "creationDate": "Sun Oct 14 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "completionDate": null,
          "dueDate": null,
          "remindDate": null,
          "description": null,
          "completed": false,
          "starred": true,
          "subtasks": []
        },
        {
          "id": "xch7RcCCzi",
          "listId": "cCbGroWCzi",
          "title": "Add schema to new version of the website",
          "creationDate": "Tue Oct 16 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "completionDate": null,
          "dueDate": null,
          "remindDate": null,
          "description": null,
          "completed": false,
          "starred": false,
          "subtasks": []
        }
      ]
    },
    {
      "id": "CbGraoWCzi",
      "title": "Email Marketing",
      "tasks": [
        {
          "id": "duS1nlq4lr",
          "listId": "CbGraoWCzi",
          "title": "Fix photoshop images",
          "creationDate": "Wed Oct 17 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "completionDate": "Thu Oct 18 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "dueDate": null,
          "remindDate": null,
          "description": null,
          "completed": true,
          "starred": false,
          "subtasks": []
        },
        {
          "id": "eRnIR8dYAf",
          "listId": "CbGraoWCzi",
          "title": "Send test mail",
          "creationDate": "Mon Oct 22 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "completionDate": "Mon Oct 22 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "dueDate": null,
          "remindDate": null,
          "description": null,
          "completed": true,
          "starred": false,
          "subtasks": []
        },
        {
          "id": "f77Kjez8y8",
          "listId": "CbGraoWCzi",
          "title": "Try different encoding",
          "creationDate": "Mon Oct 22 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "completionDate": "Tue Oct 23 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "dueDate": null,
          "remindDate": null,
          "description": null,
          "completed": true,
          "starred": false,
          "subtasks": []
        },
        {
          "id": "x1LRYqhkxa",
          "listId": "CbGraoWCzi",
          "title": "Fix HTML bugs",
          "creationDate": "Tue Oct 23 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "completionDate": null,
          "dueDate": "Fri Oct 26 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "remindDate": null,
          "description": null,
          "completed": true,
          "starred": false,
          "subtasks": []
        },
        {
          "id": "xchf7RcCzi",
          "listId": "CbGraoWCzi",
          "title": "Configure Mailchimp",
          "creationDate": "Tue Oct 16 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "completionDate": null,
          "dueDate": "Tue Oct 30 2018 12:10:00 GMT-0300 (Horário Padrão de Brasília)",
          "remindDate": null,
          "description": null,
          "completed": false,
          "starred": false,
          "subtasks": []
        }
      ]
    }
  ]
};