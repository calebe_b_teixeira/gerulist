import Vue from 'vue'
import Datepicker from 'vuejs-datepicker';

// List
import List from '@/components/List/List.vue';
import ListAlert from '@/components/List/ListAlert.vue';

// Task
import Tasks from '@/components/Task/Tasks.vue';
import Task from '@/components/Task/Task.vue';
import AddTask from '@/components/Task/AddTask.vue';

// Sidebar
import Sidebar from '@/components/Sidebar/Sidebar.vue';
import SidebarItem from '@/components/Sidebar/SidebarItem.vue';
import SidebarItemNewList from '@/components/Sidebar/SidebarItemNewList.vue';

// Taskbar
import Taskbar from '@/components/Taskbar/Taskbar.vue';
import TaskbarDateItem from '@/components/Taskbar/TaskbarDateItem.vue';
import TaskbarDeleteItem from '@/components/Taskbar/TaskbarDeleteItem.vue';

// Subtask
import Subtasks from '@/components/Taskbar/Subtasks.vue';
import Subtask from '@/components/Taskbar/Subtask.vue';

// Content
import Content from '@/components/Content/Content.vue';

Vue.component('Datepicker', Datepicker);
Vue.component('List', List);
Vue.component('ListAlert', ListAlert);
Vue.component('Tasks', Tasks);
Vue.component('Task', Task);
Vue.component('AddTask', AddTask);
Vue.component('Sidebar', Sidebar);
Vue.component('SidebarItem', SidebarItem);
Vue.component('SidebarItemNewList', SidebarItemNewList);
Vue.component('Taskbar', Taskbar);
Vue.component('TaskbarDateItem', TaskbarDateItem);
Vue.component('TaskbarDeleteItem', TaskbarDeleteItem);
Vue.component('Subtasks', Subtasks);
Vue.component('Subtask', Subtask);
Vue.component('Content', Content);