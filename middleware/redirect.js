export default function ({ store, redirect }) {
  return redirect('/lists/inbox')
}